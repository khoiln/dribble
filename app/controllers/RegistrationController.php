<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Mailers\UserMailer as Mailer;
use Dribble\Validator\UserValidator;
use Dribble\Repository\UsersRepository;

class RegistrationController extends BaseController
{
    public function __construct(Mailer $mailer, UserValidator $validator, UsersRepository $user)
    {
        $this->mailer = $mailer;
        $this->validator = $validator;
        $this->user = $user;
    }

    public function validateUser($username, $code)
    {
        if (!$this->user->activate($username, $code))
            return Redirect::to('gallery')->with('flashError', t('You are not registered with us'));

        return Redirect::to('gallery')->with('flashSuccess', t('Your account is now activated'));
    }

    public function getIndex()
    {
        return View::make('registration/index')
            ->with('title', 'Registration');
    }

    public function postIndex()
    {
        if (!$this->validator->validRegistration(Input::all()))
            return Redirect::to('registration')->withErrors($this->validator->errors());

        if (!$this->user->createNew(Input::all()))
            return Redirect::to('registration')->with('flashError', 'Please try again, enable to create user');

        return Redirect::to('login')->with('flashSuccess', t('A confirmation email is sent to your mail'));
    }

    public function getFacebook()
    {
        if (!Session::get('facebookDetails')) {
            return Redirect::to('login')->with('flashError', t('Please try again'));
        }
        return View::make('registration/facebook')
            ->with('title', 'Facebook Login');
    }

    public function postFacebook()
    {
        $session = Session::get('facebookDetails');
        if (!$session) {
            return Redirect::to('login');
        }

        if (!$this->validator->validFacebookRegistration(Input::all())) {
            return Redirect::to('registration/facebook')->withErrors($this->validator->errors());
        }

        if ($this->user->createFacebookUser(Input::all(), $session)) {
            return Redirect::to('gallery')->with('flashSuccess', t('Congratulations your account is created and activated'));
        }
    }

    public function getGoogle()
    {
        if (Session::get('googleDetails'))
            return View::make('registration/google')->with('title', t('Registration'));

        return Redirect::to('login')->with('flashError', t('Please try again'));
    }

    public function postGoogle()
    {
        $session = Session::get('googleDetails');
        if (!$session) {
            return Redirect::to('login')->with('flashError', t('Please try again'));
        }

        if (!$this->validator->validGoogleRegistration(Input::all())) {
            return Redirect::to('registration/google')->withErrors($this->validator->errors());
        }

        if ($this->user->createGoogleUser(Input::all(), $session)) {
            return Redirect::to('gallery')->with('flashSuccess', t('Congratulations your account is created and activated'));
        }
    }
}