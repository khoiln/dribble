<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\ImagesRepository;

class DownloadController extends BaseController
{
    public function __construct(ImagesRepository $image)
    {
        $this->image = $image;
    }

    public function getDownload($id, $slug = '')
    {
        if (Auth::check() == FALSE) {
            return Redirect::to('login')->with('flashError', t('You need to login to download images'));
        }

        $id = Crypt::decrypt($id);
        $slug = Crypt::decrypt($slug);
        $image = $this->image->get($id);
        if (!$image) {
            return Redirect::to('gallery')->with('flashError', t('You are not allowed to download this image'));
        }
        if (siteSettings('allowDownloadOriginal') == 'leaveToUser' AND $image->allow_download != '1') {
            return Redirect::to('gallery')->with('flashError', t('You are not allowed to download this image'));
        }
        if (Auth::user()->id != $image->user_id) {
            $image->downloads = $image->downloads + 1;
            $image->save();
        }
        return Response::download('uploads/' . $image->image_name . '.' . $image->type, $image->slug . '.' . $image->type, array('content-type' => 'image/jpg'));
    }


}