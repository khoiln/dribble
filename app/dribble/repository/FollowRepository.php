<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Repository;


use Follow;
use DB, Auth, File, Request;
use Dribble\Notifier\FollowNotifier;
use User;

class FollowRepository
{
    public function __construct(FollowNotifier $notice, Follow $follow)
    {
        $this->follow = $follow;
        $this->notifications = $notice;
    }

    public function follow($id)
    {
        if (Auth::user()->id == $id) {
            return t("Can't follow");
        }
        if (User::where('id', '=', $id)->count() != 1) {
            return t("Can't follow");
        }

        // Check if following
        // IF true then un-follow
        $isFollowing = $this->follow->where('user_id', '=', Auth::user()->id)->where('follow_id', '=', $id);
        if ($isFollowing->count() >= 1) {
            $isFollowing->delete();
            return t('Un-Followed');
        }

        $this->follow->user_id = Auth::user()->id;
        $this->follow->follow_id = $id;
        $this->follow->save();
        // Send notice to user who is getting followed
        $this->notifications->follow(User::find($id), Auth::user());
        return t('Following');
    }
}