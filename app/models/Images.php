<?php

/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
class Images extends Eloquent
{

    protected $table = 'images';
    protected $softDelete = TRUE;

    public static function scopeApproved()
    {
        return static::where('approved', '=', 1);
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function comments()
    {
        return $this->hasMany('Comment', 'image_id');
    }

    public function favorite()
    {
        return $this->hasMany('Favorite', 'image_id');
    }

    public function category()
    {
        return $this->belongsTo('Category', 'category_id');
    }

}