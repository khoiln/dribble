<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

namespace Controllers\Admin\Images;

use Images, View, Category, Cache, File, Str, Input, Redirect, Request;

class UpdateController extends \BaseController
{
    public function postApprove()
    {
        $image           = Images::find(Input::get('id'));
        $image->approved = 1;
        $image->save();
        return 'Approved';
    }

    public function postDisapprove()
    {
        $image = Images::where('id', '=', Input::get('id'))->first();
        if (!$image) {
            return false;
        }

        File::delete('uploads/' . $image->image_name . '.' . $image->type);
        $image->delete();
        $image->comments()->delete();
        $image->favorite()->delete();
        Cache::forget('moreFromSite');
        return 'Removed';

    }


    public function postEdit($id)
    {
        $image = Images::where('id', '=', $id)->first();
        Cache::forget('featuredImage');
        Cache::forget('moreFromSite');
        if (Input::get('delete')) {
            File::delete('uploads/' . $image->image_name . '.' . $image->type);
            $image->favorite()->delete();
            $image->comments()->delete();
            $image->delete();
            return Redirect::to('admin')->with('flashSuccess', 'Image is now deleted permanently');
        }
        if (Category::where('id', '=', Input::get('category'))->count() != 1)
            return Redirect::back()->with('flashError', t('Invalid category'));

        $slug = @Str::slug(Input::get('title'));
        if (!$slug) {
            $slug = Str::random(9);
        }
        $image->title             = Input::get('title');
        $image->slug              = $slug;
        $image->image_description = Input::get('description');
        $image->category_id       = Input::get('category');
        $image->tags              = Input::get('tags');

        if (Input::get('featured') == TRUE) {
            $image->is_featured = 1;
        } else {
            $image->is_featured = NULL;
        }

        $image->save();

        return Redirect::back()->with('flashSuccess', t('Image is now updated'));
    }
}