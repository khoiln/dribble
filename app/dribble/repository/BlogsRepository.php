<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Repository;

use Blogs;

class BlogsRepository
{
    public function __construct(Blogs $blogs)
    {
        $this->blogs = $blogs;
    }

    public function get($id)
    {
        return $this->blogs->where('id', '=' , $id)->with('user')->first();
    }

    public function getLatestBlogs($paginate = NULL)
    {
        $blogs = $this->blogs->orderBy('created_at', 'desc')->with('user');
        if (!$paginate) return $blogs->paginate(perPage());
        if ($paginate) return $blogs->paginate($paginate);

    }
}