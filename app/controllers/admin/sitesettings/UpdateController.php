<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Controllers\Admin\SiteSettings;

use view, DB, Cache, Redirect, App, Input, Validator, Str;
use Category, Images, User, Blogs;

class UpdateController extends \BaseController
{

    public function updateSettings()
    {
        DB::table('sitesettings')
            ->where('option', 'siteName')
            ->update(array('value' => Input::get('sitename')));
        DB::table('sitesettings')
            ->where('option', 'description')
            ->update(array('value' => Input::get('description')));
        DB::table('sitesettings')
            ->where('option', 'favIcon')
            ->update(array('value' => Input::get('favicon')));
        DB::table('sitesettings')
            ->where('option', 'privacy')
            ->update(array('value' => Input::get('privacy')));
        DB::table('sitesettings')
            ->where('option', 'faq')
            ->update(array('value' => Input::get('faq')));
        DB::table('sitesettings')
            ->where('option', 'tos')
            ->update(array('value' => Input::get('tos')));
        DB::table('sitesettings')
            ->where('option', 'about')
            ->update(array('value' => Input::get('about')));

        Cache::forget('siteName');
        Cache::forget('description');
        Cache::forget('favIcon');
        Cache::forget('faq');
        Cache::forget('privacy');
        Cache::forget('tos');
        Cache::forget('about');

        return Redirect::back()->with('flashSuccess', 'Site Details Updated');
    }

    public function postLimitSettings()
    {
        DB::table('sitesettings')
            ->where('option', 'numberOfImagesInGallery')
            ->update(array('value' => (int)Input::get('numberOfImages')));
        DB::table('sitesettings')
            ->where('option', 'autoApprove')
            ->update(array('value' => Input::get('autoApprove')));
        DB::table('sitesettings')
            ->where('option', 'limitPerDay')
            ->update(array('value' => (int)Input::get('limitPerDay')));
        DB::table('sitesettings')
            ->where('option', 'tagsLimit')
            ->update(array('value' => (int)Input::get('tagsLimit')));
        DB::table('sitesettings')
            ->where('option', 'allowDownloadOriginal')
            ->update(array('value' => Input::get('allowDownloadOriginal')));
        DB::table('sitesettings')
            ->where('option', 'maxImageSize')
            ->update(array('value' => Input::get('maxImageSize')));
        Cache::forget('numberOfImagesInGallery');
        Cache::forget('autoApprove');
        Cache::forget('limitPerDay');
        Cache::forget('tagsLimit');
        Cache::forget('allowDownloadOriginal');
        Cache::forget('maxImageSize');
        return Redirect::back()->with('flashSuccess', 'Your limits are now updated');
    }

    public function createSiteCategory()
    {
        $v = array('addnew' => array('required'));
        $v = Validator::make(Input::all(), $v);
        if ($v->fails()) {
            return Redirect::to('admin/sitecategory')->with('flashError', 'Please provide some input');
        }
        $category = new Category();
        $category->name = ucfirst(Input::get('addnew'));
        $slug = Str::slug(Input::get('addnew'));
        if (!$slug) {
            $slug = Str::random(9);
        }
        $category->slug = $slug;
        $category->save();
        Cache::forget('categories');
        return Redirect::to('admin/sitecategory')->with('flashSuccess', 'New Category Is Added');
    }

    public function updateSiteMap()
    {
        $sitemap = App::make("sitemap");
        $blogs = Blogs::orderBy('created_at', 'desc')->get();
        foreach ($blogs as $blog) {
            $sitemap->add(url('blog/' . $blog->id . '/' . $blog->slug), $blog->updated_at, '0.9');
        }
        $posts = Images::orderBy('created_at', 'desc')->get();
        foreach ($posts as $post) {
            $sitemap->add(url('image/' . $post->id . '/' . $post->slug), $post->updated_at, '0.9');
        }
        $users = User::orderBy('created_at', 'desc')->get();
        foreach ($users as $user) {
            $sitemap->add(url('user/' . $user->username), $user->updated_at, '0.5');
        }
        $sitemap->store('xml', 'sitemap');
        return Redirect::to('admin')->with('flashSuccess', 'sitemap.xml is now updated');
    }

    public function reorderSiteCategory()
    {
        $tree = Input::get('tree');
        foreach ($tree as $k => $v) {
            if ($v["depth"] == -1) continue;
            if ($v["parent_id"] == "root") $v["parent_id"] = 0;

            $category = Category::where('id', '=', $v['item_id'])->first();
            $category->parent_id = $v['parent_id'];
            $category->depth = $v["depth"];
            $category->lft = $v["left"] - 1;
            $category->rgt = $v["right"] - 1;
            $category->save();
        }
        Cache::forget('categories');
    }

    public function updateSiteCategory()
    {
        $v = array(
            'id'   => array('required'),
            'slug' => array('required', 'alpha_dash'),
            'name' => array('required')
        );
        $v = Validator::make(Input::all(), $v);
        if ($v->fails())
            return Redirect::to('admin/sitecategory')->withErrors($v);
        $id = Input::get('id');
        $category = Category::where('id', '=', $id)->first();
        $delete = Input::get('delete');
        if($delete) {
            $category->delete();
            return Redirect::to('admin/sitecategory')->with('flashSuccess', 'Category is now deleted');
            Cache::forget('categories');
        }

        $category->slug = Input::get('slug');
        $category->name = Input::get('name');
        $category->save();
        Cache::forget('categories');
        return Redirect::to('admin/sitecategory')->with('flashSuccess', 'Category is now updated');

    }
}