<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
class Comment extends Eloquent
{
    protected $table = 'comments';
    protected $softDelete = TRUE;

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function reply()
    {
        return $this->hasMany('Reply');
    }

    public function image()
    {
        return $this->belongsTo('Images', 'image_id');
    }
}