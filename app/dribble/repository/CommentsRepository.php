<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Repository;

use Images;
use Comment;
use Auth;
use Dribble\Notifier\ImageNotifer;

class CommentsRepository
{
    public function __construct(ImageNotifer $notifications, Comment $comment, Images $images)
    {
        $this->notifications = $notifications;
        $this->comment = $comment;
        $this->images = $images;
    }

    public function postComment($id, $input)
    {
        $image = $this->images->approved()->where('id', '=', $id)->first();
        if (!$image) {
            return FALSE;
        }
        $this->comment->user_id = Auth::user()->id;
        $this->comment->image_id = $id;
        $this->comment->comment = $input['comment'];
        $this->comment->save();
        if (Auth::user()->id != $image->user_id) {
            $this->notifications->comment($image, Auth::user(), $input['comment']);
        }
        return TRUE;
    }

    public function deleteComment($id)
    {
        $commentOwner = $this->comment->where('id', '=', $id)->first();
        if (!$commentOwner) return FALSE;
        if ($commentOwner->user_id == Auth::user()->id || Auth::user()->id == $commentOwner->image->user->id) {
            $commentOwner->delete();
            return TRUE;
        }
        return FALSE;
    }

    public function createReply() {

    }
}