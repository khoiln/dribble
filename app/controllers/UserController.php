<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\UsersRepository;
use Dribble\Repository\ImagesRepository;

class UserController extends BaseController
{
    public function __construct(UsersRepository $user, ImagesRepository $images)
    {
        $this->user = $user;
        $this->images = $images;

    }

    public function getUser($user)
    {
        $user = $this->user->get($user);

        if (!$user) return Redirect::to('gallery');

        // Get all images of this user
        $images = $this->images->getUsersImages($user->id);

        // Get most used tags from helper function
        $freq = mostTags($user->images()->lists('tags'));

        return View::make('user/index')
            ->with('title', $user->fullname)
            ->with('images', $images)
            ->with('mostUsedTags', $freq)
            ->with('user', $user);

    }


    public function getFavorites($user)
    {
        $user = $this->user->getUsersFavorites($user);
        $freq = mostTags($user->images()->lists('tags'));
        return View::make('user/favorites')
            ->with('title', $user->fullname)
            ->with('mostUsedTags', $freq)
            ->with('user', $user);

    }

    public function getFollowers($username)
    {
        $user = $this->user->getUsersFollowers($username);
        if (!$user) {
            return Redirect::to('/');
        }
        $freq = mostTags($user->images()->lists('tags'));
        return View::make('user/followers')
            ->with('title', $user->fullname)
            ->with('mostUsedTags', $freq)
            ->with('user', $user);
    }

    public function getFollowing($username)
    {
        $user = $this->user->getUsersFollowing($username);
        if (!$user) {
            return Redirect::to('/');
        }
        if ($user->id != Auth::user()->id) {
            return Redirect::to('/');
        }
        $freq = mostTags($user->images()->lists('tags'));
        return View::make('user/following')
            ->with('title', $user->fullname)
            ->with('mostUsedTags', $freq)
            ->with('user', $user);
    }

    public function getRss($user)
    {
        return $this->user->getUsersRss($user);
    }

    public function getAll()
    {
        $users = $this->user->getAll();
        return View::make('user/users')
            ->with('users', $users)
            ->with('title', 'Users');
    }
}