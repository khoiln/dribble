<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Repository;

use Illuminate\Support\Facades\Input;
use Images;
use DB, Auth, File, Cache, Str;
use Favorite;
use Dribble\Notifier\ImageNotifer;

class ImagesRepository
{
    public function __construct(Images $images, ImageNotifer $notice)
    {
        $this->images = $images;
        $this->notification = $notice;
    }

    public function get($id)
    {
        return $this->images->approved()->where('id', $id)->with('user', 'comments', 'comments.reply', 'favorite')->first();
    }

    public function getByCategory($categoryId, $limit = NULL)
    {
        $image = $this->images->approved()->where('category_id', '=', $categoryId)->orderBy('created_at', 'desc')->with('user','comments','favorite');
        if (!$limit) return $image->paginate(perPage());
        if ($limit) return $image->limit($limit);
    }

    public function getUsersImages($userId)
    {
        return $this->images->approved()->where('user_id', '=', $userId)->with('comments', 'favorite', 'user')->orderBy('created_at', 'desc')->paginate(perPage());
    }

    public function getLatestImages($limit = NULL)
    {
        $image = $this->images->approved()->orderBy('created_at', 'desc')->with('user', 'comments', 'favorite');
        if (!$limit) return $image->paginate(perPage());
        if ($limit) return $image->limit($limit);
    }

    public function getFeatured($limit = NULL)
    {
        $image = $this->images->approved()->where('is_featured',1)->orderBy('created_at', 'dec')->with('user', 'comments', 'favorite');
        if (!$limit) return $image->paginate(perPage());
        if ($limit) return $image->limit($limit);
    }

    public function getMost($type = '', $limit = NULL, $oderBy = 'desc')
    {
        switch ($type) {
            case "commented":
                $images = $this->images->with('user', 'comments', 'favorite')->approved()->join('comments', 'comments.image_id', '=', 'images.id')
                    ->select('images.*', DB::raw('count(comments.image_id) as cmts'))
                    ->groupBy('images.id')->with('user', 'comments', 'favorite')->orderBy('cmts', $oderBy);
                break;
            case "popular":
                $images = $this->images->approved()->join('comments', 'comments.image_id', '=', 'images.id')
                    ->select('images.*', DB::raw('count(comments.image_id)*5 + images.views as popular'))
                    ->groupBy('images.id')->with('user', 'comments', 'favorite')->orderBy('popular', $oderBy);
                break;
            case "favorited":
                $images = $this->images->approved()->join('favorite', 'favorite.image_id', '=', 'images.id')
                    ->select('images.*', DB::raw('count(favorite.image_id) as favs'))
                    ->groupBy('images.id')->with('user', 'comments', 'favorite')->orderBy('favs', $oderBy);
                break;
            case "downloaded":
                $images = $this->images->approved()->orderBy('downloads', $oderBy)->with('user', 'comments', 'favorite');
                break;
            case "viewed":
                $images = $this->images->approved()->orderBy('views', $oderBy)->with('user', 'comments', 'favorite');
                break;
            default:
                $images = $this->images->with('user', 'comments', 'favorite')->approved();
        }
        if (!$limit) return $images->paginate(perPage());
        if ($limit) return $images->limit($limit);
    }

    public function update(array $input, $id)
    {
        $parts = explode(',', $input['tags'], siteSettings('tagsLimit'));
        $tags = implode(',', array_map('strtolower', $parts));
        $slug = @Str::slug($input['title']);
        if(!$slug) {
        	$slug = Str::random(8);
        }
        $image = $this->images->where('id', '=', $id)->first();
        $image->title = $input['title'];
        $image->slug = $slug;
        $image->image_description = $input['description'];
        $image->category_id = $input['category'];
        $image->tags = $tags;
        $image->save();
    }

    public function delete($id)
    {
        $image = $this->images->where('id', '=', $id)->first();
        if (!$image) return FALSE;
        if ($image->user->id == Auth::user()->id) {
            File::delete('uploads/' . $image->image_name . '.' . $image->type);
            $image->delete();
            $image->comments()->delete();
            $image->favorite()->delete();
            Cache::forget('moreFromSite');
            return TRUE;
        }
    }

    public function createFavorite($id)
    {
        $fav = Favorite::where('image_id', '=', $id)->where('user_id', '=', Auth::user()->id);
        if ($fav->count() >= 1) {
            $fav->delete();
            return t('Un-Favorited');
        }
        $fav = new Favorite();
        $fav->user_id = Auth::user()->id;
        $fav->image_id = $id;
        $fav->save();
        $this->notification->favorite($this->images->where('id', $id)->first(), Auth::user());
        return t('Favorited');
    }

    public function getByTags($tag, $limit = NULL)
    {
        $images = $this->images->where('tags', 'LIKE', '%' . $tag . '%')->orderBy('created_at', 'desc')->with('user');
        if (!$limit) return $images->paginate(perPage());
        return $images->get();
    }

    public function incrementViews($image) {
        $image->views = $image->views + 1;
        $image->save();
        return $image;
    }
}