<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\ImagesRepository;
use Dribble\Validator\ImageValidator;

class ImageController extends BaseController
{

    public function __construct(ImagesRepository $image, \Dribble\Validator\ImageValidator $validator)
    {
        $this->image = $image;
        $this->validator = $validator;
    }

    /**
     * Display all details of image with it's comments
     * and replies send it view file.
     * @param $id
     * @param null $slug
     * @return mixed
     */
    public function getIndex($id, $slug = NULL)
    {
        $image = $this->image->get($id);

        // If image does not exist then return to base url
        if (!$image) return Redirect::to('/');

        // If image slug is empty or slug is not correct
        // Then redirect to image page with correct slug
        if (empty($slug) || $slug != $image->slug) return Redirect::to('image/' . $image->id . '/' . $image->slug);

        // If image is not approvied then return to base url
        if ($image->approved == 0) return Redirect::to('/');

        // Fire an event
        Event::fire('image.views',$image);

        // Get all the image commentes and paginate them
        $comments = $image->comments()->with('user', 'reply')->orderBy('created_at', 'desc')->paginate(10);

        $exif = NULL;
        if (extension_loaded('exif')) {
            $exif = @exif_read_data(public_path() . '/uploads/' . $image->image_name . '.' . $image->type);
        }
        return View::make('image/index')
            ->with('image', $image)
            ->with('exif', $exif)
            ->with('comments', $comments)
            ->with('title', ucfirst($image->title));
    }

    public function delete($id)
    {
        // If image delete is success then return to gallery with success notfication
        if ($this->image->delete($id))
            return Redirect::to('gallery')->with('flashSuccess', t('Image is deleted permanently'));

        return Redirect::to('gallery')->with('flashError', t('You are not allowed to download this image'));
    }

    public function getEdit($id, $slug = NULL)
    {
        $image = $this->image->get($id);

        // If image does not exist then return to base url
        if (!$image || Auth::user()->id !== $image->user_id) return Redirect::to('/');

        // If image slug is empty or slug is not correct
        // Then redirect to image page with correct slug
        if (empty($slug) || $slug != $image->slug) return Redirect::to('image/' . $image->id . '/' . $image->slug);

        // If image is not approvied then return to base url
        if ($image->approved == 0) return Redirect::to('/');


        return View::make('image/edit')
            ->with('image', $image)
            ->with('title', ucfirst($image->title));
    }

    public function postEdit($id)
    {
        if (!$this->validator->validUpdate(Input::all()))
            return Redirect::back()->withErrors($this->validator->errors());

        if (Category::where('id', '=', Input::get('category'))->count() != 1)
            return Redirect::back()->with('flashError', t('Invalid category'));

        $image = $this->image->get($id);
        // If image does not exist then return to base url
        if (!$image || Auth::user()->id !== $image->user_id) return Redirect::to('/');

        $data = array(
            'title'       => Input::get('title'),
            'description' => Input::get('description'),
            'category' => Input::get('category'),
            'tags'        => Input::get('tags')
        );
        $this->image->update($data, $id);

        $image = $this->image->get($id);
        return Redirect::to('image/' . $image->id . '/' . $image->slug)->with('flashSuccess', t('Image is now updated'));
    }
}
