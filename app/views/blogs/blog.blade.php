@extends('master/index')

@section('content')
<h3 class="content-heading">{{ t('Blog') }}</h3>
<h1 class="blog-title"><a href="{{ url('blog/'.$blog->id.'/'.$blog->slug) }}">{{ ucfirst($blog->title) }}</a></h1>
<p class="blog-meta">{{ t('Published by') }} <a href="{{ url('user/'.$blog->user->username) }}">{{ ucfirst($blog->user->fullname) }}</a> &middot; <abbr class="timeago comment-time" title="{{ date(DATE_ISO8601,strtotime($blog->created_at)) }}">{{ date(DATE_ISO8601,strtotime($blog->created_at)) }}</abbr></p>
<p>{{ $blog->description }}</p>
@stop
