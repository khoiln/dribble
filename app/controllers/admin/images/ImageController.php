<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

namespace Controllers\Admin\Images;

use Images, View, Category, Cache, File, Str, Input, Redirect, Request;

class ImageController extends \BaseController
{
    public function getImagesList()
    {
        $sortBy = Request::get('sortBy');
        $direction = Request::get('direction');
        if ($sortBy && $direction)
            $images = Images::approved()->with('user', 'favorite')->orderBy($sortBy, $direction)->paginate(50);
        else
            $images = Images::approved()->with('user', 'favorite')->paginate(50);

        return View::make('admin/images/index')
            ->with('images', $images)
            ->with('title', 'List of all images');
    }

    public function featuredImagesList()
    {
        $sortBy = Request::get('sortBy');
        $direction = Request::get('direction');
        if ($sortBy && $direction)
            $images = Images::approved()->where('is_featured', '=', '1')->orderBy($sortBy, $direction)->paginate(50);
        else
            $images = Images::approved()->where('is_featured', '=', '1')->paginate(50);
        return View::make('admin/images/index')
            ->with('images', $images)
            ->with('title', 'List of featured Images');
    }


    public function getEdit($id)
    {
        $image = Images::where('id', '=', $id)->with('favorite', 'user')->first();
        return View::make('admin/images/edit')->with('image', $image);
    }

    public function getImagesApproval()
    {
        $images = Images::where('approved', '=', 0)->where('deleted_at', '=', NULL)->with('user', 'favorite')->paginate(50);

        return View::make('admin/images/approve')
            ->with('images', $images)
            ->with('title', 'Images Required Approval');
    }

}