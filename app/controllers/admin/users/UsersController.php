<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Controllers\Admin\Users;

use User, View, Redirect, Input, Validator, Hash, Request;

class UsersController extends \BaseController
{
    public function getUsersList()
    {
        $sortBy    = Request::get('sortBy');
        $direction = Request::get('direction');

        if ($this->sort($sortBy, $direction))
            $users = User::orderBy($sortBy, $direction)->paginate(50);
        else
            $users = User::paginate(50);

        return View::make('admin/users/users')
            ->with('users', $users)
            ->with('title', 'All users');
    }

    public function getFeaturedUserList()
    {
        $sortBy    = Request::get('sortBy');
        $direction = Request::get('direction');

        if ($this->sort($sortBy, $direction))
            $users = User::withTrashed()->orderBy($sortBy, $direction)->where('is_featured', '=', '1')->paginate(50);
        else
            $users = User::withTrashed()->where('is_featured', '=', '1')->paginate(50);

        return View::make('admin/users/users')
            ->with('users', $users)
            ->with('title', 'Featured Users');
    }

    public function getBannedUserList()
    {
        $sortBy    = Request::get('sortBy');
        $direction = Request::get('direction');

        if ($this->sort($sortBy, $direction))
            $users = User::withTrashed()->orderBy($sortBy, $direction)->where('permission', '=', 'ban')->paginate(50);
        else
            $users = User::withTrashed()->where('permission', '=', 'ban')->paginate(50);

        return View::make('admin/users/users')
            ->with('users', $users)
            ->with('title', 'Banned Users');
    }

    public function getEditUser($user)
    {
        $user = User::where('username', '=', $user)->first();

        if (!$user) return Redirect::to('admin')->with('flashError', 'User does not exist');

        return View::make('admin/users/edit')
            ->with('user', $user);
    }


    private function sort($sortBy, $direction)
    {
        $sortArray      = array('username', 'fullname', 'created_at', 'updated_at');
        $directionArray = array('asc', 'desc');
        if (in_array($sortBy, $sortArray) && in_array($direction, $directionArray))
            return true;
    }

    public function getAddUser()
    {
        return View::make('admin/users/add');
    }

}