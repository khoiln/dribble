<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Validator;

use Images;

class ImageValidator extends Validator
{
    protected $imageRules = array(
        'title'    => array('required', 'max:200'),
        'category' => array('required'),
        'tags'     => array('required')
    );

    protected $updateRules = array(
        'title'    => array('required', 'max:200'),
        'category' => array('required'),
        'tags'     => array('required')
    );

    public function __construct(Images $model)
    {
        $this->model = $model;
    }

}