<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Notifier;

use Dribble\Mailers\ImageMailer;
use User;
use Images;

class ReplyNotifer extends Notifier
{

    public function __construct(ImageMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function replyNotice(User $to, User $from, Images $on, $reply, $sendEmail = FALSE)
    {
        $this->sendNew($to->id, $from->id, 'reply', $on->id);
        if ($sendEmail === TRUE) {
            $this->mailer->replyMail($to, $from, $on, $reply);
        }
    }
}