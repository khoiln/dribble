<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

namespace Dribble\Validator;

use User;

class UserValidator extends Validator
{
    protected $registrationRules = array(
        'username'                 => array('required', 'min:3', 'max:20', 'alpha_num', 'Unique:users'),
        'fullname'                 => array('required', 'min:3', 'max:80'),
        'gender'                   => array('required', 'in:male,female'),
        'email'                    => array('required', 'between:3,64', 'email', 'unique:users'),
        'password'                 => array('required', 'between:4,25', 'confirmed'),
        'password_confirmation'    => array('required', 'between:4,25'),
        'recaptcha_response_field' => array('required', 'recaptcha')
    );

    protected $facebookRegistrationRules = array(
        'username'              => array('required', 'min:3', 'max:20', 'alpha_num', 'unique:users'),
        'password'              => array('required', 'between:4,25', 'confirmed'),
        'password_confirmation' => array('required', 'between:4,25'),
    );

    protected $googleRegistrationRules = array(
        'username'              => array('required', 'min:3', 'max:20', 'alpha_num', 'unique:users'),
        'password'              => array('required', 'between:4,25', 'confirmed'),
        'password_confirmation' => array('required', 'between:4,25'),
    );

    protected $updateRules = array(
        'fullname' => array('required'),
        'gender'   => array('required', 'in:male,female'),
        'country'  => array('required', 'alpha_num'),
        'dob'      => array('date_format:Y-m-d'),
        'blogurl'  => array('url'),
        'fb_link'  => array('url'),
        'tw_link'  => array('url')
    );

    protected $passwordRestRules = array(
        'email'                    => array('required', 'email'),
        'recaptcha_response_field' => array('required', 'recaptcha'),
    );

    protected $passwordUpdateRule = array(
        'password'              => array('required', 'min:6', 'confirmed'),
        'currentpassword'       => array('required'),
        'password_confirmation' => array('required', 'between:4,25')
    );

    public function __construct(User $model)
    {
        $this->model = $model;
    }
}