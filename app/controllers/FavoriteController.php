<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\ImagesRepository;

class FavoriteController extends BaseController
{
    public function __construct(ImagesRepository $image)
    {
        $this->image = $image;
    }

    public function postFavorite()
    {
        if (Auth::check() == FALSE) {
            return 'Login First';
        }
        $v = array(
            'id' => array('required', 'integer')
        );
        $v = Validator::make(Input::all(), $v);
        if ($v->fails()) {
            return t('Not Allowed');
        }

        return $this->image->createFavorite(Input::get('id'));
    }


}