<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Notifier;

use Dribble\Mailers\UserMailer;
use User;
use Images;

class FollowNotifier extends Notifier
{
    public function __construct(UserMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function follow(User $to, User $from)
    {
        $this->sendNew($to->id, $from->id, 'follow', NULL);

        $this->mailer->followMail($to, $from);
    }
}
