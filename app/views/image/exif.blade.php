@if(isset($exif['Model']) || isset($exif['FocalLength']) || exif_get_shutter($exif) || isset($exif['COMPUTED']['ApertureFNumber']) || isset($exif['COMPUTED']['ApertureFNumber']) || isset($exif['ISOSpeedRatings']))
<h3 class="block-heading">EXIF DATA</h3>

<div class="clearfix exif">
    @if(isset($exif['Model']))
    <p><strong>Model </strong> {{ $exif['Model'] }}</p>
    @endif
    @if(isset($exif['FocalLength']))
    <p><strong>Focal Length</strong> {{ exif_focal($exif['FocalLength']) }}mm</p>
    @endif
    @if(exif_get_shutter($exif))
    <p><strong>Shutter Speed</strong> {{ exif_get_shutter($exif) }}</p>
    @endif
    @if(isset($exif['COMPUTED']['ApertureFNumber']))
    <p><strong>Aperture</strong> {{ $exif['COMPUTED']['ApertureFNumber']}}</p>
    @endif
    @if(isset($exif['ISOSpeedRatings']))
    <p><strong>ISO/Film</strong> {{ $exif['ISOSpeedRatings']}}</p>
    @endif
    @if(getGPS($exif))
    <div id="gmaps" style="height:200px;"></div>
    @endif
</div>

@section('extrafooter')
@if(getGPS($exif))
{{ HTML::script('http://maps.google.com/maps/api/js?sensor=true') }}
{{ HTML::script('static/js/gmaps.js') }}
<script type="text/javascript">
    var map;
    $(function(){
        map = new GMaps({
            div: '#gmaps',
            lat: {{ getGPS($exif)['lat'] }},
            lng: {{ getGPS($exif)['long'] }}
        });
        map.addMarker({
            lat: {{ getGPS($exif)['lat'] }},
            lng: {{ getGPS($exif)['long'] }}
        });
    });
</script>
@endif
@stop
@endif
