<hr>
<footer>
    <p class="pull-right"><a href="#">{{ t('Back to top') }}</a></p>

    <p>&copy; {{ date("Y") }} {{ siteSettings('siteName') }}&nbsp;&middot;&nbsp;
    		<a href="{{ url('blogs') }}">{{ t('Blogs') }}</a>&nbsp;&middot;&nbsp;
            <a href="{{ url('privacy') }}">{{ t('Privacy Policy') }}</a>&nbsp;&middot;&nbsp;
            <a href="{{ url('tos') }}">{{ t('Terms') }}</a>&nbsp;&middot;&nbsp;
            <a href="{{ url('faq') }}">{{ t('FAQ') }}</a>&nbsp;&middot;&nbsp;
            <a href="{{ url('about') }}">{{ t('About Us') }}</a>
        @include('master/language')
    </p>
</footer>