<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

Route::get('/', 'HomeController@getIndex')->before('guest');
Route::get('gallery', 'GalleryController@getIndex');
Route::get('image/{id}/{slug?}', 'ImageController@getIndex')->where(array('id' => '\d+'));
Route::get('user/{username}', 'UserController@getUser');
Route::get('user/{username}/favorites', 'UserController@getFavorites');
Route::get('user/{username}/followers', 'UserController@getFollowers');
Route::get('user/{username}/rss', 'UserController@getRss');
Route::get('users', 'UserController@getAll');
Route::get('category/{category}', 'CategoryController@getIndex');
Route::get('category/{category}/rss', 'CategoryController@getRss');
Route::get('tag/{tag}', 'TagController@getIndex');
Route::get('tag/{tag}/rss', 'TagController@getRss');
Route::get('notifications', 'NotificationController@getIndex');
Route::get('search', 'SearchController@getIndex');
Route::get('tos', 'PolicyController@getTos');
Route::get('privacy', 'PolicyController@getPrivacy');
Route::get('faq', 'PolicyController@getFaq');
Route::get('about', 'PolicyController@getAbout');
Route::get('featured', 'MostController@featured');
Route::get('popular', 'MostController@mostPopular');
Route::get('most/viewed', 'MostController@mostViewed');
Route::get('most/commented', 'MostController@mostCommented');
Route::get('most/favorites', 'MostController@mostFavorited');
Route::get('most/downloads', 'MostController@mostDownloaded');
Route::get('blogs', 'BlogsController@getIndex');
Route::get('blog/{id}/{slug}', 'BlogsController@getBlog');
Route::get('lang/{lang?}', function ($lang) {
    if (in_array($lang, languageArray())) {
        Session::put('my.locale', $lang);
    } else {
        Session::put('my.locale', 'en');
    }
    return Redirect::to('/');
});
Route::post('queue/receive', function(){ 
	return Queue::marshal(); 
});

/**
 * Guest only visit this section
 */
Route::group(array('before' => 'guest'), function () {
    Route::get('login', 'LoginController@getLogin');
    // Facebook
    Route::get('get/facebook','LoginController@loginWithFacebook');
    Route::get('registration/facebook','RegistrationController@getFacebook');
    // Google
    Route::get("get/google",'LoginController@loginWithGoogle');
    Route::get("registration/google",'RegistrationController@getGoogle');

    Route::controller('password','RemindersController');
    Route::get('registration', 'RegistrationController@getIndex');
    Route::get('registration/activate/{username}/{code}', 'RegistrationController@validateUser');
});

/**
 * Guest Post form with csrf protection
 */
Route::group(array('before' => 'csrf|guest'), function () {
    Route::post('login', 'LoginController@postLogin');
    // Facebook
    Route::post('registration/facebook', 'RegistrationController@postFacebook');
    // Google
    Route::post('registration/google','RegistrationController@postGoogle');
    // Normal Registration
    Route::post('registration', 'RegistrationController@postIndex');
    Route::post('password/remind', 'PasswordresetController@postIndex');
    Route::post('password/reset/{token}', 'PasswordresetController@resetPassword');
});


/*
 * Ajax post
 */
Route::group(array('before' => 'ajax|ajaxban'), function () {
    Route::post('favorite', 'FavoriteController@postFavorite');
    Route::post('follow', 'FollowController@postFollow');
    Route::post('reply', 'ReplyController@postReply');
    Route::post('deletecomment', 'CommentController@postDeleteComment');
    Route::post('deletereply', 'ReplyController@postDeleteReply');
    Route::post('upload', 'UploadController@postUpload')->before('ban');
});

/*
 * Require login to access these sections
 */
Route::group(array('before' => 'auth'), function () {
    Route::get('image/{id}/{slug?}/edit', 'ImageController@getEdit')->where(array('id' => '\d+'));
    Route::get('upload', 'UploadController@getIndex')->before('ban');
    Route::get('logout', 'LoginController@getLogout');
    Route::get('feeds', 'FeedsController@getIndex');
    Route::get('user/{username}/following', 'UserController@getFollowing');
    Route::get('download/{id}/{any}', 'DownloadController@getDownload')->before('ban');
    Route::get('settings', 'SettingsController@getSettings');
    Route::get('delete/image/{id}', 'ImageController@delete');
    Route::get('report/image/{id}', 'ReportController@getReport')->before('ban');
    Route::get('report/user/{username}', 'ReportController@getReport')->before('ban');
});

/**
 * Post Sections CSRF + AUTH both
 */
Route::group(array('before' => 'csrf|auth'), function () {

    Route::post('image/{id}/{slug?}', 'CommentController@postComment')->where(array('id' => '\d+'))->before('ban');
    Route::post('image/{id}/{slug?}/edit', 'ImageController@postEdit')->where(array('id' => '\d+'))->before('ban');
    Route::post('settings/changepassword', 'SettingsController@postChangePassword');
    Route::post('settings/updateprofile', 'SettingsController@postUpdateProfile');
    Route::post('settings/mailsettings', 'SettingsController@postMailSettings');
    Route::post('settings/updateavatar', 'SettingsController@postUpdateAvatar');
    Route::post('report/image/{id}', 'ReportController@postReportImage')->before('ban');
    Route::post('report/user/{username}', 'ReportController@postReportUser')->before('ban');

});


/**
 * Admin section users with admin privileges can access this area
 */
Route::group(array('before' => 'admin', 'namespace' => 'Controllers\Admin'), function () {
    Route::get('admin', 'IndexController@getIndex');
// Users Manager
    Route::get('admin/users', 'Users\UsersController@getUsersList');
    Route::get('admin/users/featured', 'Users\UsersController@getFeaturedUserList');
    Route::get('admin/users/banned', 'Users\UsersController@getBannedUserList');
    Route::get('admin/user/{username}/edit', 'Users\UsersController@getEditUser');
    Route::get('admin/adduser', 'Users\UsersController@getAddUser');
    Route::post('admin/user/{username}/edit', 'Users\UpdateController@updateUser');
    Route::post('admin/adduser', 'Users\UpdateController@addUser');

// Image Manger
    Route::get('admin/images', 'Images\ImageController@getImagesList');
    Route::get('admin/images/featured', 'Images\ImageController@featuredImagesList');
    Route::get('admin/images/approval', 'Images\ImageController@getImagesApproval');
    Route::get('admin/image/{id}/edit', 'Images\ImageController@getEdit');
    Route::post('admin/image/{id}/edit', 'Images\UpdateController@postEdit');
    Route::post('admin/images/approve', 'Images\UpdateController@postApprove');
    Route::post('admin/images/disapprove', 'Images\UpdateController@postDisapprove');

// Site Settings
    Route::get('admin/sitesettings', 'SiteSettings\SettingsController@getSiteDetails');
    Route::get('admin/sitecategory', 'SiteSettings\SettingsController@getSiteCategory');
    Route::get('admin/limitsettings', 'SiteSettings\SettingsController@getLimitSettings');
    Route::get('admin/removecache', 'SiteSettings\SettingsController@getRemoveCache');
    Route::get('admin/updatesitemap', 'SiteSettings\UpdateController@updateSiteMap');
    Route::post('admin/sitesettings', 'SiteSettings\UpdateController@updateSettings');
    Route::post('admin/limitsettings', 'SiteSettings\UpdateController@postLimitSettings');
    Route::post('admin/sitecategory', 'SiteSettings\UpdateController@createSiteCategory');
    Route::post('admin/sitecategory/reorder', 'SiteSettings\UpdateController@reorderSiteCategory');
    Route::post('admin/sitecategory/update', 'SiteSettings\UpdateController@updateSiteCategory');
// Comments
    Route::get('admin/comments', 'Comments\CommentsController@getComments');
    Route::get('admin/comment/{id}/edit', 'Comments\CommentsController@getEditComment');
    Route::post('admin/comment/{id}/edit', 'Comments\CommentsController@postEditComment');


// Blogs
    Route::get('admin/blogs', 'Blogs\BlogsController@getBlogs');
    Route::get('admin/blog/create', 'Blogs\BlogsController@getCreate');
    Route::get('admin/blog/{id}/edit', 'Blogs\BlogsController@getEdit');
    Route::post('admin/blog/create', 'Blogs\BlogsController@postCreate');
    Route::post('admin/blog/{id}/edit', 'Blogs\BlogsController@postEdit');

//Bulk upload
    Route::get('admin/bulkupload', 'Bulkupload\BulkuploadController@getBulkUpload');
    Route::post('admin/bulkupload', 'Bulkupload\BulkuploadController@postBulkUpload');

// Reports
    Route::get('admin/reports', 'Reports\ReportsController@getReports');
    Route::get('admin/report/{id}', 'Reports\ReportsController@getReadReport');

});