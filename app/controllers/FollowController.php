<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\FollowRepository;

class FollowController extends BaseController
{

    public function __construct(FollowRepository $follow)
    {
        $this->follow = $follow;
    }

    /**
     * User can follow other user
     * If already following then un-follow
     * Also sends notification to user
     * Via ajax
     */
    public function postFollow()
    {
        if (Auth::check() == FALSE) return t('Login');

        if (!Request::ajax()) return t('can\'t follow');

        return $this->follow->follow(Input::get('id'));
    }
}