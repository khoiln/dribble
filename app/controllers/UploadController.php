<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Validator\ImageValidator;

class UploadController extends BaseController
{
    public function __construct(ImageValidator $validator)
    {
        $this->validator = $validator;
    }

    private function error($str)
    {
        return array('files' => array(
            0 => $str
        ));
    }

    public function getIndex()
    {
        return View::make('upload/index')
            ->with('title', t('Upload'));
    }

    public function postUpload()
    {
        $uploadedImage = Input::file('files')[0];

        if (@getimagesize($uploadedImage) == FALSE)
            return $this->error(array('error' => t('Image File is required')));

        if (!$this->validator->validImage(Input::all()))
            return $this->error($this->validator->messages()->toArray());

        // check if category exits
        if (Category::where('id', '=', Input::get('category'))->count() != 1)
            return $this->error(array('error' => t('Invalid category')));

        if (Auth::user()->images()->where('created_at', '>=', date('Y-m-d'))->count() >= limitPerDay())
            return $this->error(array('error' => t('You have reached today\'s limit')));

        if ($uploadedImage->getSize() >= (siteSettings('maxImageSize') * 1048576))
            return $this->error(array('error' => t('File is too large, max size allowed is ') . siteSettings('maxImageSize') . 'MB'));

        $imageName = $this->dirName();
        $mimetype = $uploadedImage->getMimeType();
        $mimetype = preg_replace('/image\//', '', $mimetype);
        $file = $uploadedImage->move('uploads/', $imageName . '.' . $mimetype);

        $tags = Input::get('tags');
        $parts = explode(',', $tags, siteSettings('tagsLimit'));

        if (count($parts) == 0)
            return $this->error(array('error' => t('Tags are required')));

        if (strlen($parts[0]) == 0)
            return $this->error(array('error' => t('Tags are required')));

        $tags = implode(',', array_map('strtolower', $parts));

        $format_description = preg_replace('/\R\R+/u', "\n\n", trim(Input::get('description')));

        $slug = @Str::slug(Input::get('title'));

        if (strlen($slug) <= '1') {
            $slug = str_random(9);
        }
        $allowDownload = 1;
        if (siteSettings('allowDownloadOriginal') == 'leaveToUser') {
            if (preg_match('/\b(0|1)\b/', Input::get('allowDownloadOriginal'))) {
                $allowDownload = Input::get('allowDownloadOriginal');
            } else {
                $allowDownload = 1;
            }
        }
        if (Auth::user()->is_featured == 1) {
            $approve = 1;
        } elseif (siteSettings('autoApprove') == '0') {
            $approve = 0;
        } else {
            $approve = 1;
        }
        $upload = new Images();
        $upload->user_id = Auth::user()->id;
        $upload->image_name = $imageName;
        $upload->title = Input::get('title');
        $upload->slug = $slug;
        $upload->category_id = Input::get('category');
        $upload->type = $mimetype;
        $upload->tags = $tags;
        $upload->image_description = $format_description;
        $upload->allow_download = $allowDownload;
        $upload->approved = $approve;

        $upload->save();


        Cache::forget('moreFromSite');

        if ((int)siteSettings('autoApprove') == 0) {
            return $this->error(array('success' => t('Your image is uploaded, require approval please keep patience'), 'thumbnail' => asset('uploads/' . $upload->image_name . '.' . $upload->type)));
        }
        return $this->error(array('success' => 'Uploaded', 'successSlug' => url('image/' . $upload->id . '/' . $upload->slug), 'successTitle' => ucfirst($upload->title), 'thumbnail' => asset('uploads/' . $upload->image_name . '.' . $upload->type)));
    }

    private function dirName()
    {
        $str = str_random(9);
        if (file_exists(public_path() . '/uploads/' . $str)) {
            $str = $this->dirName();
        }
        return $str;
    }
}