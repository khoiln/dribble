<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Mailers;

use Illuminate\Support\Facades\Config;
use Mail;

abstract class Mailer
{
    public function sendTo($user, $subject, $view, $data = array())
    {
        if (Config::get('mail.password')) {
            Mail::queue($view, $data, function ($message) use ($user, $subject) {
                $message->to($user->email)->subject($subject);
            });
        }
    }
}