<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Validator\CommentValidator;
use Dribble\Repository\CommentsRepository;

class CommentController extends BaseController
{

    public function __construct(CommentValidator $validator, CommentsRepository $comments)
    {
        $this->validator = $validator;
        $this->comments = $comments;
    }

    public function postDeleteComment()
    {
        // If comment delete success then return success
        if ($this->comments->deleteComment(Input::get('id')))
            return 'success';

        return 'failed';
    }

    public function postComment($id, $slug)
    {
        // Check if request if from a valid image page
        if (Request::is('image/*/*') == FALSE)
            return Redirect::to('gallery')->with('flashNotice', t('You are not allowed'));

        // If not a valid comment then return to image page with errors
        if (!$this->validator->validComment(Input::all()))
            return Redirect::to('image/' . $id . '/' . $slug)->withErrors($this->validator->errors());

        // If commment creating is not success then return error
        if (!$this->comments->postComment($id, Input::all()))
            return Redirect::to('gallery')->with('flashError', t('You are not allowed'));

        return Redirect::to('image/' . $id . '/' . $slug)->with('flashSuccess', t('Your comment is added'));
    }
}