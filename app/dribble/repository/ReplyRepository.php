<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Repository;


use Images, User;
use Reply;
use Auth;
use Dribble\Notifier\ReplyNotifer;
use Comment;

class ReplyRepository
{
    public function __construct(ReplyNotifer $notifications, Reply $reply, Comment $comment, Images $images)
    {
        $this->notification = $notifications;
        $this->reply = $reply;
        $this->comment = $comment;
        $this->images = $images;
    }

    public function createReply($input)
    {
        $comment = $this->comment->where('id', '=', $input['reply_msgid'])->first();
        if (!$comment)
            return 'Not allowed';
        if (!$this->images->where('id', '=', $comment->image_id)->first())
            return 'Not allowed';

        $this->reply->user_id = Auth::user()->id;
        $this->reply->image_id = $comment->image_id;
        $this->reply->comment_id = $comment->id;
        $this->reply->reply = $input['textcontent'];
        $this->reply->save();

        if ($this->reply->comment->user->id != Auth::user()->id)
            $this->notification->replyNotice($this->reply->comment->user, Auth::user(), $comment->image, $input['textcontent'], TRUE);

        $noticeSentToUsers = array();
        foreach ($this->reply->where('comment_id', '=', $input['reply_msgid'])->get() as $replier) {
            if ($replier->user_id != Auth::user()->id AND $replier->user_id != $comment->user_id && !in_array($replier->user_id, $noticeSentToUsers)) {
                $this->notification->replyNotice($replier->user, Auth::user(), $comment->image, $input['textcontent']);
                $noticeSentToUsers[] = $replier->user_id;
            }
        }
    }

    public function deleteReply($input)
    {
        $reply = $this->reply->where('id', '=', $input)->first();
        if(!$reply) return false;
        if ($reply->user_id == Auth::user()->id || $reply->comment->user->id == Auth::user()->id || $reply->image->user->id == Auth::user()->id) {
            $reply->delete();
            return TRUE;
        }
    }
}