@extends('master/index')
@section('metaDescription')
@if(strlen($image->description) > 2)
<meta name="description" content="{{ $description }}">
@else
<meta name="description" content="{{ $image->title}} {{ siteSettings('description') }}">
@endif
<meta property="og:title" content="{{ ucfirst($image->title) }} - {{ siteSettings('siteName') }}"/>
<meta property="og:image" content="{{ asset(cropResize('uploads/'.$image->image_name. '.' . $image->type )) }}"/>
@stop


@section('content')
<h3 class="content-heading">Editing Image</h3>
{{ Form::open() }}
<div class="form-group">
    <label for="title">Title</label>
    {{ Form::text('title',$image->title,array('class'=>'form-control','required'=>'required')) }}
</div>
<div class="form-group">
    <label for="title">Description</label>
    {{ Form::textarea('description',$image->image_description,array('class'=>'form-control')) }}
</div>
<div class="form-group">
    <label for="category">Category</label>
    <select name="category" class="form-control" required>
        <option value="{{ $image->category->id }}">{{ ucfirst($image->category->name) }}</option>
        @foreach(siteCategories() as $category)
        <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <input type="text" autocomplete="off" name="tags" placeholder="Tags" class="form-control tm-input tm-input-success" data-original-title=""/>
</div>

<div class="form-group">
    {{ Form::submit('Update Image',array('class'=>'btn btn-default')) }}
</div>
{{ Form::close() }}
@stop

@section('sidebar')
@include('image/sidebar')
@stop

@section('extrafooter')
<script>
    var tagApi = jQuery(".tm-input").tagsManager({
        prefilled: [@foreach(explode(',',$image->tags) as $tag) "{{ $tag }}", @endforeach],
        delimiters: [9, 13, 44, 32],
        maxTags:{{ (int)siteSettings('tagsLimit')}}
    });
</script>
@stop