<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

namespace Dribble\Validator;

use Reply;

class ReplyValidator extends Validator
{
    protected $replyRules = array(
        'reply_msgid' => array('required'),
        'textcontent' => array('required')
    );

    public function __construct(Reply $model)
    {
        $this->model = $model;
    }
}