<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\ImagesRepository;

class GalleryController extends BaseController
{
    public function __construct(ImagesRepository $images)
    {
        $this->images = $images;
    }

    /**
     * Main gallery of site
     */
    public function getIndex()
    {
        $images = $this->images->getLatestImages();
        return View::make('gallery/index')
            ->with('images', $images)
            ->with('title', t('Gallery'));
    }
}