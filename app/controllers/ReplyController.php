<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Validator\ReplyValidator;
use Dribble\Repository\ReplyRepository;

class ReplyController extends BaseController
{
    public function __construct(ReplyValidator $validator, ReplyRepository $reply)
    {
        $this->validator = $validator;
        $this->reply = $reply;
    }

    public function postDeleteReply()
    {
        if (Auth::check() == FALSE)
            return 'Login first';

        if ($this->reply->deleteReply(Input::get('id')))
            return 'success';

        return 'false';
    }

    public function postReply()
    {
        if (Auth::check() == FALSE)
            return 'Login first';

        if (!$this->validator->validReply(Input::all()))
            return 'Not allowed';

        $this->reply->createReply(Input::all());

        return '<div class="media"> <hr> <a class="pull-left bla" href="' . url('user/' . Auth::user()->username) . '"> <img class="media-object" src="' . avatar(Auth::user()->avatar, 75, 75) . '"> </a> <div class="media-body"> <h4 class="media-heading comment"><a href="' . url('user/' . Auth::user()->username) . '">' . ucfirst(Auth::user()->fullname) . '</a>    </h4> ' . Input::get('textcontent') . ' </div> </div>';
    }
}