<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Mailers;

use User, Images;

class ImageMailer extends Mailer
{

    public function commentMail(User $to, User $sender, $comment, $link)
    {
        if (!$to->email_comment) return;

        $subject = "New Comment";
        $view = 'emails.usermailer.comment';
        $data = array(
            'fullname' => ucfirst($to->fullname),
            'from'     => ucfirst($sender->fullname),
            'comment'  => $comment,
            'link'     => $link
        );

        return $this->sendTo($to, $subject, $view, $data);
    }

    public function replyMail(User $to, User $from, Images $on, $reply)
    {
        if (!$to->email_reply) return;

        $subject = 'New Reply';
        $view = 'emails.usermailer.reply';
        $data = array(
            'senderFullname'              => ucfirst($from->fullname),
            'senderProfileLink' => url('user/' . $from->username),
            'imageLink'         => url('image/' . $on->id . '/' . $on->slug),
            'reply'             => $reply,
        );
        return $this->sendTo($to, $subject, $view, $data);
    }

    public function favoriteMail(User $to, User $from, Images $on)
    {
        if (!$to->email_favorite) return;

        $subject = 'Favorited';
        $view = 'emails.usermailer.favorite';
        $data = array(
            'from'  => ucfirst($from->fullname),
            'title' => ucfirst($on->title),
            'link'  => url('image/' . $on->id . '/' . $on->slug)
        );
        return $this->sendTo($to, $subject, $view, $data);
    }
}