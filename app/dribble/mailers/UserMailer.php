<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Mailers;

use User, Images;

class UserMailer extends Mailer
{
    public function activation(User $user, $activationCode)
    {
        $subject = "Welcome";
        $view = 'emails.registration.welcome';
        $data = array(
            'fullname'       => $user->fullname,
            'username'       => $user->username,
            'activationcode' => $activationCode
        );
        return $this->sendTo($user, $subject, $view, $data);
    }


    public function followMail(User $to, User $from)
    {
        if (!$to->email_follow) return;

        $subject = "New Follower";
        $view = 'emails.usermailer.follow';
        $data = array(
            'senderFullname'    => ucfirst($from->fullname),
            'senderProfileLink' => url('user/' . $from->username)
        );
        return $this->sendTo($to, $subject, $view, $data);
    }

}