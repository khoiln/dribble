<?php

/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
class Category extends Eloquent
{
    protected $table = 'categories';

    public function images()
    {
        $this->hasMany('Images', 'category_id');
    }
}