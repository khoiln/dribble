<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
namespace Dribble\Repository;

use User;
use Images;
use Feed;
use Hash;
use Auth;
use URL;
use Dribble\Mailers\UserMailer;

class UsersRepository
{

    public function __construct(UserMailer $mailer, User $user)
    {
        $this->user = $user;
        $this->mailer = $mailer;
    }

    public function get($username)
    {
        $user = $this->user->where('username', $username)->with('followers.user')->first();
        if (!$user) {
            return FALSE;
        }
        return $user;
    }

    public function getAll()
    {
        return $this->user->confirmed()->with('latestImages', 'comments')->paginate(perPage());
    }


    public function getUsersFavorites($username)
    {
        return $this->user->where('username', '=', $username)->with('favorites.image.user')->first();
    }

    public function getUsersFollowers($username)
    {
        return $this->user->where('username', '=', $username)->with('followers')->first();
    }

    public function getUsersFollowing($username)
    {
        return $this->user->where('username', '=', $username)->with('following.followingUser')->first();
    }

    public function createNew(array $input)
    {
        $activationCode = sha1(str_random(11) . (time() * rand(2, 2000)));

        $this->user->username = $input['username'];
        $this->user->fullname = $input['fullname'];
        $this->user->gender = $input['gender'];
        $this->user->email = $input['email'];
        $this->user->password = Hash::make($input['password']);
        $this->user->confirmed = $activationCode;
        $this->user->save();

        $this->mailer->activation($this->user, $activationCode);
        return TRUE;
    }

    public function edit()
    {

    }

    public function activate($username, $activationCode)
    {
        $user = $this->user->where('username', '=', $username)->first();
        if ($user->confirmed === $activationCode) {
            $user->confirmed = 1;
            $user->save();
            return TRUE;
        }
        return FALSE;
    }

    public function createFacebookUser(array $input, $session = NULL)
    {
        if (!$session) return;
        $user = new User();
        $user->username = $input['username'];
        $user->password = Hash::make($input['password']);
        $user->fbid = $session['id'];
        $user->email = $session['email'];
        if ($session['gender'])
            $user->gender = $session['gender'];
        $user->fullname = $session['name'];
        $user->confirmed = 1;
        $user->save();
        Auth::loginUsingId($user->id);
        return TRUE;
    }

    public function createGoogleUser(array $input, $session = NULL)
    {
        if (!$session) return;
        $user = new User();
        $user->username = $input['username'];
        $user->password = Hash::make($input['password']);
        $user->gid = $session['id'];
        $user->email = $session['email'];
        if ($session['gender'])
            $user->gender = $session['gender'];
        $user->fullname = $session['name'];
        $user->confirmed = 1;
        $user->save();
        Auth::loginUsingId($user->id);
        return TRUE;
    }

    public function getUsersRss($username)
    {
        $user = User::where('username', '=', $username)->first();
        $images = Images::approved()->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->paginate(perPage());

        $feed = Feed::make();
        $feed->title = siteSettings('siteName') . '/user/' . $user->username;
        $feed->description = siteSettings('siteName') . '/user/' . $user->username;
        $feed->link = URL::to('user/' . $user->username);
        $feed->lang = 'en';

        foreach ($images as $post) {
            $desc = '<a href="' . url('image/' . $post->id . '/' . $post->slug) . '"><img src="' . asset(cropResize('uploads/' . $post->image_name . '.' . $post->type)) . '" /></a><br/><br/>
                <h2><a href="' . url('image/' . $post->id . '/' . $post->slug) . '">' . $post->title . '</a>
                by
                <a href="' . url('user/' . $user->username) . '">' . ucfirst($user->fullname) . '</a>
                ( <a href="' . url('user/' . $user->username) . '">' . $user->username . '</a> )
                </h2>' . $post->image_description;
            $feed->add(ucfirst($post->title), $user->fullname, URL::to('image/' . $post->id . '/' . $post->slug), $post->created_at, $desc);
        }
        return $feed->render('rss');
    }


    public function updateProfile($input)
    {
        $update = Auth::user();
        $update->fullname = $input['fullname'];
        $update->dob = $input['dob'];
        $update->gender = $input['gender'];
        $update->country = $input['country'];
        $update->about_me = $input['about_me'];
        $update->blogurl = $input['blogurl'];
        $update->fb_link = $input['fb_link'];
        $update->tw_link = $input['tw_link'];
        $update->save();

        return TRUE;
    }

    public function updateMail($input)
    {
        // TODO : improve this
        $user = Auth::user();
        if (isset($input['emailcomment']))
            $user->email_comment = 1;
        else
            $user->email_comment = 0;

        if (isset($input['emailreply']))
            $user->email_reply = 1;
        else
            $user->email_reply = 0;

        if (isset($input['emailfavorite']))
            $user->email_favorite = 1;
        else
            $user->email_favorite = 0;

        if (isset($input['emailfollow']))
            $user->email_follow = 1;
        else
            $user->email_follow = 0;

        $user->save();
        return TRUE;
    }

    public function updatePassword($input)
    {
        if (Hash::check($input['currentpassword'], Auth::user()->password)) {
            $user = Auth::user();
            $user->password = Hash::make($input['password']);
            $user->save();
            return TRUE;
        } else {
            return FALSE;
        }
    }
}