<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

namespace Controllers\Admin;

use DB, View;

class IndexController extends \BaseController
{
    public function getIndex()
    {
//       MONTHLY SIGNUP DETAILS
        $signUpDetails = DB::select('SELECT MONTH(created_at) as month ,YEAR(created_at) as year,COUNT(id) as number
FROM users
GROUP BY YEAR(created_at), MONTH(created_at)');

        for ($i = date('n'); $i >= 0; $i--) {
            if (isset($signUpDetails[$i]->month)) {
                $newSingupDetails[$signUpDetails[$i]->month] = $signUpDetails[$i];
            }
        }

        $imageDetails = DB::select('SELECT MONTH(created_at) as month ,YEAR(created_at) as year,COUNT(id) as number
FROM images
GROUP BY YEAR(created_at), MONTH(created_at)');

        for ($i = date('n'); $i >= 0; $i--) {
            if (isset($imageDetails[$i]->month)) {
                $newImageDetails[$imageDetails[$i]->month] = $imageDetails[$i];
            }
        }

        if (!isset($newSingupDetails)) {
            $newSingupDetails = NULL;
        }
        if (!isset($newImageDetails)) {
            $newImageDetails = NULL;
        }
        return View::make('admin/sitedetails/index')
            ->with('signUpDetails', $newSingupDetails)
            ->with('imageDetails', $newImageDetails);
    }
}