<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\BlogsRepository;

class BlogsController extends BaseController
{
    public function __construct(BlogsRepository $blogs)
    {
        $this->blogs = $blogs;
    }

    public function getIndex()
    {
        return View::make('blogs/list')->with('title', t('All Blogs'))
            ->with('blogs', $this->blogs->getLatestBlogs(5));
    }

    public function getBlog($id, $slug)
    {
        $blog = $this->blogs->get($id);

        if (!$blog) return Redirect::to('/');

        if (empty($slug) || $slug != $blog->slug) return Redirect::to('blog/' . $blog->id . '/' . $blog->slug);

        return View::make('blogs/blog')->with('title', $blog->title)->with('blog', $blog);
    }
}