@extends('master/index')


@section('content')
<h3 class="content-heading">Report</h3>
{{ Form::open() }}
<div class="form-group">
    <label for="report">Reason For Reporting</label>
    {{ Form::textarea('report','',array('class'=>'form-control','id'=>'username','placeholder'=>'Enter Some Details')) }}
</div>
{{ Form::submit('Report',array('class'=>'btn btn-default'))}}
{{ Form::close() }}

@stop