<div class="col-md-3">
    <div class="clearfix">
        <h3 class="content-heading">{{ t('Share This') }}</h3>
        @include('master/share')
    </div>

    <div class="clearfix">
        <h3 class="content-heading">{{ t('Featured Image') }}</h3>

        <div class="imagesFromUser">
            @foreach(getFeaturedImage() as $featuredImage)
            <a href="{{ url('image/'.$featuredImage->id.'/'.$featuredImage->slug) }}" class="pull-left userimage">
                <img src="{{ asset('uploads/'.$featuredImage->image_name.'.' . $featuredImage->type) }}"
                     alt="{{ $featuredImage->title }}" class="thumbnail">
            </a>
            @endforeach
        </div>
    </div>

    @if(getFeaturedUser()->count() >= 1)
    <div class="clearfix">
        <h3 class="content-heading">{{ t('Featured User') }}</h3>

        <div class="imagesFromUser">
            @foreach(getFeaturedUser() as $featuredUser)
            <div class="col-md-12">
                <div class="row">

                    <a href="{{ url('user/'.$featuredUser->username) }}" class="thumbnail pull-left">
                        <img src="{{ avatar($featuredUser->avatar,69,69) }}" alt="{{ $featuredUser->fullname }}">
                    </a>

                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p><strong><a href="{{ url('user/'.$featuredUser->username) }}">{{ $featuredUser->fullname }}</a></strong></p>
                        @if(Auth::check())
                        @if(checkFollow($featuredUser->id))
                        <button class="btn btn-default btn-xs replyfollow follow" id="{{ $featuredUser->id }}">{{ t('Un-Follow') }}</button>
                        @else
                        <button class="btn btn-default btn-xs replyfollow follow" id="{{ $featuredUser->id }}">{{ t('Follow-Me') }}</button>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif


    <div class="clearfix">
        <h3 class="block-heading">{{ t('More From') }} {{ siteSettings('siteName') }}</h3>
        <div class="more-from-site">
            @foreach(moreFromSite() as $sidebarImage)
            <a href="{{ url('image/'.$sidebarImage->id.'/'.$sidebarImage->slug) }}"><img src="{{ asset('uploads/'.$sidebarImage->image_name.'.' . $sidebarImage->type ) }}" alt="{{ $sidebarImage->title }}"/></a>
            @endforeach
        </div>
    </div>
</div>