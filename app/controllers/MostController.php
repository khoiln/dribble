<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Repository\ImagesRepository;

class MostController extends BaseController
{

    public function __construct(ImagesRepository $images)
    {
        $this->images = $images;
    }

    public function featured()
    {
        $images = $this->images->getFeatured();
        return View::make('gallery/index')
            ->with('images', $images)
            ->with('title', t('Featured Images'));
    }

    public function mostCommented()
    {
        $images = $this->images->getMost('commented');
        return View::make('gallery/index')
            ->with('images', $images)
            ->with('title', t('Most Commented'));
    }

    public function mostFavorited()
    {
        $images = $this->images->getMost('favorited');
        return View::make('gallery/index')
            ->with('images', $images)
            ->with('title', t('Most Favorites'));
    }

    public function mostDownloaded()
    {
        $images = $this->images->getMost('downloaded');
        return View::make('gallery/index')
            ->with('images', $images)
            ->with('title', t('Most Downloads'));
    }

    public function mostPopular()
    {
        $images = $this->images->getMost('popular');
        return View::make('gallery/index')
            ->with('images', $images)
            ->with('title', t('Popular'));
    }

    public function mostViewed()
    {
        $images = $this->images->getMost('viewed');
        return View::make('gallery/index')
            ->with('images', $images)
            ->with('title', t('Most Viewed'));
    }
}