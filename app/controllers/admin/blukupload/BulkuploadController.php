<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

namespace Controllers\Admin\Bulkupload;

use Images, Category, View, Cache, File, Str, Input, Redirect, Request, Auth, DB;

class BulkuploadController extends \BaseController
{
    public function getBulkUpload()
    {
        return View::make('admin/bulkupload/index');
    }

    public function postBulkUpload()
    {
        // check if category exits
        if (Category::where('id', '=', Input::get('category'))->count() != 1) {
            return $this->error(array('error' => t('Invalid category')));
        }

        $imageName = $this->dirName();
        $mimetype = Input::file('files')[0]->getMimeType();
        $mimetype = preg_replace('/image\//', '', $mimetype);
        $title = Input::file('files')[0]->getClientOriginalName();
        $title = str_replace(array('.jpg', '.gif', '.png', '.jpeg', '.JPG', '.GIF', '.PNG', '.JPEG'), '', $title);
        $file = Input::file('files')[0]->move('uploads/', $imageName . '.' . $mimetype);
        $tags = Input::get('tags');
        $parts = explode(',', $tags, siteSettings('tagsLimit'));

        if (count($parts) == 0) {
            return $this->error(array('error' => 'Tags are required'));
        }
        if (strlen($parts[0]) == 0) {
            return $this->error(array('error' => 'Tags are required'));
        }
        $tags = implode(',', array_map('strtolower', $parts));

        $slug = @Str::slug($title);
        if (strlen($slug) <= '1') {
            $slug = str_random(9);
        }

        $upload = new Images();
        $upload->user_id = Auth::user()->id;
        $upload->image_name = $imageName;
        $upload->title = $title;
        $upload->slug = $slug;
        $upload->category_id = Input::get('category');
        $upload->type = $mimetype;
        $upload->tags = $tags;
        $upload->allow_download = 1;
        $upload->approved = 1;

        $upload->save();

        return $this->error(array('success' => 'Uploaded', 'successSlug' => url('image/' . $upload->id . '/' . $upload->slug), 'successTitle' => ucfirst($upload->title), 'thumbnail' => asset('uploads/' . $upload->image_name . '.' . $upload->type)));
    }

    private function error($str)
    {
        return array('files' => array(
            0 => $str
        ));
    }

    private function dirName()
    {
        $str = str_random(9);
        if (file_exists(public_path() . '/uploads/' . $str)) {
            $str = $this->dirName();
        }
        return $str;
    }
}