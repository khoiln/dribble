@extends('master/index')
@section('custom')
@include('user/rightsidebar')
<div class="col-md-9">
<style>
    .follwing {
        margin-bottom: 10px;
    }
</style>
<h3 class="content-heading">{{ t('Users')}} I'm following</h3>

@foreach($user->following as $follower)
<div class="col-md-4 br-right follwing clearfix">
    <a href="{{ url('user/'.$follower->followingUser->username) }}" class="pull-left user-profile-avatar">
        <img src="{{ avatar($follower->followingUser->avatar,120,120) }}" alt="...">
    </a>
    <h4>{{{ $follower->followingUser->fullname }}}<br>
        <small>{{{ $follower->followingUser->username }}}</small>
    </h4>
    @if(Auth::check() == true)
    @if(checkFollow($follower->followingUser->id))
    <a type="button" class="btn btn-info btn-xs  follow" id="{{ $follower->followingUser->id }}">{{ t('Un Follow') }}</a>
    @else
    <a type="button" class="btn btn-info btn-xs  follow" id="{{ $follower->followingUser->id }}">{{ t('Follow Me') }}</a>
    @endif
    @endif
</div>

@endforeach
</div>
@stop
@section('sidebar')
@stop