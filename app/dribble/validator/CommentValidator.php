<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */

namespace Dribble\Validator;

use Comment;

class CommentValidator extends Validator
{
    protected $commentRules = array(
        'comment' => array('required', 'min:2')
    );

    public function __construct(Comment $model)
    {
        $this->model = $model;
    }
}