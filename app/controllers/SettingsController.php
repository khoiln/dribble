<?php
/**
 * @author Khoi Lai <khoi.geeky@gmail.com>
 */
use Dribble\Validator\UserValidator;
use Dribble\Repository\UsersRepository;

class SettingsController extends BaseController
{

    public function __construct(UserValidator $validator, UsersRepository $user)
    {
        $this->validator = $validator;
        $this->user = $user;
    }

    public function getSettings()
    {
        $user = Auth::user()->find(Auth::user()->id);
        return View::make('settings/index')
            ->with('user', $user)
            ->with('title', t('Settings'));
    }

    public function postUpdateAvatar()
    {

        $imageName = $this->dirName();
        $file = Image::open(Input::file('avatar'))->cropResize(400, 400)->save('avatar/' . $imageName . '.jpg', 'jpg', 90);
        $update = Auth::user();
        $update->avatar = $imageName;
        $update->save();
        return Redirect::to('settings')->with('flashSuccess', t('Your avatar is now updated'));
    }

    private function dirName()
    {
        $str = str_random(9);
        if (file_exists(public_path() . '/avatar/' . $str)) {
            $str = $this->dirName();
        }
        return $str;
    }

    public function postUpdateProfile()
    {
        if (!$this->validator->validUpdate(Input::all()))
            return Redirect::to('settings')->withErrors($this->validator->errors())->withInput(Input::all());

        if (countryIsoCodeMatch(Input::get('country')) == FALSE) {
            return Redirect::to('settings')->with('flashError', t('Invalid country'))->withInput(Input::all());
        }

        if (strlen(Input::get('blogurl')) > 2) {
            if (!preg_match('/^(http(?:s)?\:\/\/[a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+)*\.[a-zA-Z]{2,6}(?:\/?|(?:\/[\w\-]+)*)(?:\/?|\/\w+\.[a-zA-Z]{2,4}(?:\?[\w]+\=[\w\-]+)?)?(?:\&[\w]+\=[\w\-]+)*)$/', Input::get('blogurl')))
                return Redirect::to('settings')->with('flashError', t('Invalid Blog Url'));
        }

        $data = array(
            'fullname' => Input::get('fullname'),
            'dob'      => Input::get('dob'),
            'gender'   => Input::get('gender'),     
            'country'  => Input::get('country'),
            'about_me' => Input::get('aboutme'),
            'blogurl'  => Input::get('blogurl'),
            'fb_link'  => Input::get('fbLink'),
            'tw_link'  => Input::get('twLink')
        );
        $this->user->updateProfile($data);
        return Redirect::to('settings')->with('flashSuccess', t('Your profile is updated'));
    }

    public function postChangePassword()
    {
        if (!$this->validator->validPasswordUpdate(Input::all()))
            return Redirect::to('settings')->withErrors($this->validator->errors());

        if (!$this->user->updatePassword(Input::all()))
            return Redirect::to('settings')->with('flashError', t('Old password is not valid'));

        return Redirect::to('settings')->with('flashSuccess', t('Your password is updated'));
    }

    public function postMailSettings()
    {
        $this->user->updateMail(Input::all());

        return Redirect::to('settings')->with('flashSuccess', t('Your mail settings are now update'));
    }
}