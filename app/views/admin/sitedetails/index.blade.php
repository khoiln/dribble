@extends('admin/master')
@section('extra-css')
<link rel="stylesheet" href="http://startbootstrap.com/templates/sb-admin-v2/css/plugins/morris/morris-0.4.3.min.css"/>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">
            <small><i class="fa fa-dashboard"></i></small>
            Dashboard
        </h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!--Comments-->
<div class="row">
<div class="col-lg-3">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-comments fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ numberOfComments() }}</p>

                    <p class="announcement-text">Comments</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <a href="{{ url('admin/comments') }}">
                        <div class="col-xs-6">
                            View Comments
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </a>
                  
                </div>
            </div>
        </a>
    </div>
</div>

<!--Images-->
<div class="col-lg-3">
    <div class="panel panel-warning">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-picture-o fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ numberOfImages() }}</p>

                    <p class="announcement-text">Images</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <a href="{{ url('admin/images') }}">
                        <div class="col-xs-6">
                            View Images
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </a>
                </div>
            </div>
        </a>
    </div>
</div>

<!--Users-->
<div class="col-lg-3">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-users fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ numberOfUsers() }}</p>

                    <p class="announcement-text">Users</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <a href="{{ url('admin/users') }}">
                        <div class="col-xs-6">
                            View All Users
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </a>
                </div>
            </div>
        </a>
    </div>
</div>

<!--Feature Images-->
<div class="col-lg-3">
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-star-o fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ numberOfFeaturedImages() }}</p>

                    <p class="announcement-text">Featured Images</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <a href="{{ url('admin/images/featured') }}">
                        <div class="col-xs-6">
                            Featured Images
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </a>
                </div>
            </div>
        </a>
    </div>
</div>

<!--Blogs-->
<div class="col-lg-3">
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-file-text fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ Blogs::count() }}</p>

                    <p class="announcement-text">Blogs</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <a href="{{ url('admin/blogs') }}">
                        <div class="col-xs-6">
                            View Blogs
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </a>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="col-lg-3">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-frown-o fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ numberOfBanedUsers() }}</p>

                    <p class="announcement-text">Banned Users</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <div class="col-xs-6">
                        View Banned Users
                    </div>
                    <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="col-lg-3">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-flag-o fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ numberOfReports() }}</p>

                    <p class="announcement-text">Reports</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <a href="{{ url('admin/reports') }}"><div class="col-xs-6">
                        View Reports
                    </div>
                    <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                    </div></a>
                </div>
            </div>
        </a>
    </div>
</div>


@if(siteSettings('autoApprove') == 0)
<div class="col-lg-3">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-question fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{ numberOfImagesApprovalRequired() }}</p>

                    <p class="announcement-text">Require Approval</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <a href="{{ url('admin/images/approval') }}"><div class="col-xs-6">
                            Require Approval
                    </div>
                    <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                    </div></a>
                </div>
            </div>
        </a>
    </div>
</div>
@endif
</div>

<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Monthy Sigup Chart
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row section">
                    <div class="col-md-12">
                        <h4 class="title">Monthy Sigup Chart <small>Monthly number of registered users</small></h4>
                    </div>
                    <div class="col-md-12 chart">
                        <div id="hero-graph" style="height: 230px;"></div>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Monthy Image Uploads
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row section">
                    <div class="col-md-12">
                        <h4 class="title">Monthy Image Uploads <small>Number of images uploaded monthly</small></h4>
                    </div>
                    <div class="col-md-12 chart">
                        <div id="image-upload" style="height: 230px;"></div>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bell fa-fw"></i> Site details
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="list-group">
                    <div id="donut-example"></div>
                </div>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

        <!-- /.panel -->
    </div>
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->


@stop

@section('extra-js')
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>

<script type="text/javascript">
    Morris.Donut({
        element: 'donut-example',
        data: [
            {label: "Images", value: {{ Images::count() }} },
            {label: "Users", value: {{ User::count() }}},
            {label: "Comments", value: {{ Comment::count() }} },
            {label: "Reply", value: {{ Reply::count() }} },
        ]
    });

    var tax_data = [

    @for($i = date('n'); $i >= 0; $i--)
        @if(isset($signUpDetails[$i]))
    {"period": "{{ date('Y') }}-{{ $signUpDetails[$i]->month }}", "signups": {{ $signUpDetails[$i]->number }} },
    @else
    {"period": "{{ date('Y') }}-{{ $i }}", "signups": 0 },
    @endif
    @endfor
    ];
    Morris.Line({
        element: 'hero-graph',
        data: tax_data,
        xkey: 'period',
        xLabels: "month",
        ykeys: ['signups'],
        labels: ['Signups']
    });


    var tax_data = [

    @for($i = date('n'); $i >= 0; $i--)
        @if(isset($imageDetails[$i]))
    {"period": "{{ date('Y') }}-{{ $imageDetails[$i]->month }}", "signups": {{ $imageDetails[$i]->number }} },
    @else
    {"period": "{{ date('Y') }}-{{ $i }}", "signups": 0 },
    @endif
    @endfor
    ];
    Morris.Line({
        element: 'image-upload',
        data: tax_data,
        xkey: 'period',
        xLabels: "month",
        ykeys: ['signups'],
        labels: ['Image Uploads']
    });
</script>
@stop